import { createApp } from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';

createApp(App).use(ElementUI).mount('#app');

/**
 * vue @vue/cli 5.0.8
 * element-ui 2.15.14
 */
